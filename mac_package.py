#!/usr/bin/env python
"""
Script for building the application bundle for MacOS X.
Use py2app 0.4.2
Generate a universal application (Intel, PowerPC) for MacOS X 10.3.9 and more

Usage:
    python setup.py py2app
Require :
    Python 2.5.4
    Pygame 1.9.1
    py2app 0.4.2
"""
from setuptools import setup
from mwarbasic import * # import constants

PLIST = dict(
    CFBundleIconFile=kMWIcon,
    CFBundleName=k_NAME,
    CFBundleShortVersionString=k_VERSION,
    CFBundleGetInfoString=' '.join([k_NAME, k_VERSION]),
    CFBundleExecutable=k_NAME,
    CFBundleIdentifier=kMWId,
)

setup(
    setup_requires=["py2app"],
    app=["microwar.py"],
    data_files=[kMacOSPath, kDataPath],
    options=dict(py2app=dict(
        iconfile='%s/%s' % (kMacOSPath,kMWIcon),
        plist=PLIST,excludes=['numpy'],
    ))
)
