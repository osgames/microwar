#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
MicroWar 2.0

Constants and some utility functions

Build with :
    Python 2.5.4
    Pygame 1.9.1
    py2app 0.4.2

See Source.rtf for licence and usage
"""

#Import Modules
import os, pygame, random, pickle
from pygame.locals import *
import datetime
import time
import sys
from urllib2 import urlopen

if sys.platform=='darwin':  # for MacOS only
    import Carbon.File, Carbon.Folder, Carbon.Folders
    import MacOS

# General Constants
k_NAME='MicroWar'
k_VERSION='2.0rc3'
k_HSCORE_URL='http://microwar.sourceforge.net/high_score.php?show'
k_HSCORE_LEN=15
k_SENDSCORE_URL='http://microwar.sourceforge.net/high_score.php?create'
k_UPDATE_URL='http://microwar.sourceforge.net/microwar-update.html'
kUpdateDelay=1      # delay (days) between update check over internet (k_UPDATE_URL)
kPrefVersion=5      # preference version file
kScoreVersion=1      # highscore version file

kSCREEN_WIDTH=1024
kSCREEN_HEIGHT=768

kHighScoreLen=15    # number of high score displayed
kMaxHighScore=250    # number of high score store in local file

kNextLevel=2000
kIntroPause=1500    # pause before laucnhing a PC wave (*lines)
kIntroLevel=1000    # additionnal intro level pause
kGameOver=2000

#Layer constants (higher is front-most : 90-99 used by FGame module)
kDefaultLayer=0
kBonusLayer=1
kSpriteLayer=2
kSaucerLayer=3
kShotLayer=4

#paths
kDataPath='data'
kMacOSPath='English.lproj'
kSoundPath='sounds'
kSpritePath='sprites'
kBackgroundPath='backgrounds'
kFontPath='fonts'
kDataFilename='data.ini'
kWidgetFilename='widget.ini'
kMWId='org.maddog.microwar'
kMWIcon='mwar.icns'
if sys.platform=='darwin':  # for MacOS only
    kPrefFileName="%s.ini" % kMWId
    kHighScoreFileName="%s.scores.ini" % kMWId
else:   # for other system (Linux, Windows)
    kPrefFileName='microwar.ini'
    kHighScoreFileName='microwar_scores.ini'

# Scores
kShipScoreHit=10
kShipScoreKill=20
kShipScoreBonus=1
kShipDetachScoreKill=50
kSaucerScoreHit=10
kSaucerScoreKill=50
kBonusScore=5
kBonusScoreNew=50
kLevelScoreBase=150
kLevelScoreClock=40
kLevelScoreTime=10
kPointSpeed=-0.05
kPointClkBase=500
kPointClk=5

# Player
kRamLife=4.0
kPlayerDamage=1.0
kPlayerDamagePower=3.0
kPlayerSpeedBase=0.4
kPlayerSpeedLevel=0.02
kPlayerShootSpeedBase=0.45
kPlayerShootSpeedModel=0.2
kPlayerShootSpeedSystem=0.03
kPlayerYOffset=20
kPlayerYArrivingSpeed=0.7
kPlayerScrambling=500
kPlayerZooming=150
kPlayerShootMinDelay=75

# Saucer
kSaucerRate=6.0
kSaucerMinDelay=3.0
kSaucerSpeedBase=0.15
kSaucerLife=3.0
kSaucerShootRate=2.5
kSaucerShootMinDelay=0.6
kSaucerShootSpeed=0.25
kBonusLife=0.5
kPowerUp=5
kPowerUpExplodeDistance=110
kSaucerVirus=10

# Player shoot image in the shotlist (data.ini)
kPlayerShootClassic=0
kPlayerShootIMac=1
kPlayerShootIntel=2
kPlayerShootClarus=3
kPlayerShootNewton=4
kPlayerShootIPod=5
kPlayerShootIPhone=6

# Bonus, reference to the image in the bonuslist (data.ini)
kBonusApple1=0
kBonusApple2=1
kBonus68K=2
kBonusPPC=3
kBonusIntel=4
kBonusClarus=5
kBonusNewton=6
kBonusiPod=7
kBonusiPhone=8
kBonusRam=9
kBonusHalo=10

# Ships (PC)
kShipLife=1.0
kShipDamage=2.0
kShipSpeedBaseX=0.180
kShipSpeedBaseY=1.000
kShipSpeedLevel=0.008
kShipDown=24
kShipShootRateBase=9.0
kShipShootRateModel=-1.55
kShipShootRateLevel=-0.03
kShipShootMinDelay=1.1
kShipShootSpeedBase=0.080
kShipShootSpeedModel=0.035
kShipShootSpeedLevel=0.004
kShipShootDamage=1.0
kShipGroupSpeedBase=1.000
kShipGroupSpeedSize=1.010
kShipGroupSpeedAccelerator=3.250
kShipTargetWidth=100.0
kShipTargetWidthStep=25.0
kOutScreenLimit=100.0
kShipShootWidthBonus=0.02
kShipShootRateBonus=0.75
kShipDetachRateBase=8.0
kShipDetachRateModel=-0.4
kShipDetachMinDelay=1.5
kShipDetachSpace=200.0    # space need to detach from the squad (pixels)
kShipDetachLevel=18 # PC detach level target player (2002)
kMinShipDetachHeight=300.0
kShipDetachLevelOffset=16
kShipDetachWidth=80.0
kShipDetachAngle=0.1
kShipDetachMiniWidth=4.0
kShipDetachInfectedCoef=0.0005
kShipDetachNormalCoef=0.002
kShipDetachXKill=250.0

# Ship images in screen (data.ini)
kShipVirus=0
kShipC64=1
kShipAmstrad=2
kShipAmiga=3
kShipAtari=4
kShipOS2=5
kShipMSDOS=6
kShipWin10=7
kShipWin20=8
kShipWin30=9
kShipWin95=10
kShipWin98=11
kShipWinXP=12
kShipWinVista=13
kShipWin70=14

# Ship shoot images (data.ini)
kShipShootK7=0
kShipShootDisk5=1
kShipShootDisk3=2
kShipShootCD=3

# Explosion
kExplosionAnimRate=50

# Fonts parameters
kGameFontName='emulogic.ttf'
kMenuFontName='addcityboy.ttf'
kFontBigSize=200
kFontTitle=120
kFontIntroSize=50
kFontMainSize=32
kFontSubTitleSize=24
kFontTextSize=20
kFontNormalSize=16
kFontInfoSize=8
kFontNormalColor=Color('#4bff41ff')
kFontAlternateColor=Color('#dbff39ff')
kFontLevelColor=Color('#ffff00ff')
kFontTextColor=Color('#ffffffff')
kFontAlertColor=Color('#ff0000ff')
kFontTitleColor=Color('#fd8d09ff')
kInfoTextColor=Color('#787878ff')
kFontMenuOnColor=Color('#ffed00ff')
kFontMenuOffColor=Color('#e5d500ff')
kFontMenuOnBackColor=Color('#97be0dcc')
kFontMenuOffBackColor=Color('#bac78c80')
kBlackColor=Color('#000000aa')

# life bar parameters
kLifeBackground=Color('#61846355')
kLifeHigh=Color('#00ff15aa')
kLifeMedium=Color('#ffb200aa')
kLifeLow=Color('#ff0000aa')
kLifeWidth=64
kLifeHeight=15
kLifeMargin=10
kLifeTextXOffset=-5
kLifeTextYOffset=6

# loading bar parameters
kLoadBackground=Color('#61846380')
kLoadBar=Color('#00ff00aa')
kLoadWidth=300
kLoadHeight=25
kLoadYPos=250

# player info string parameters
kInfoXPosition=10
kInfoYPosition=-60
kInfoDY=15
kInfoXIcon=22
kInfoBlink=2500
kInfoBlinkRate=250

# menu buttons parameters
kMenuButtonWidth=600
kMenuButtonMargin=10
kMenuFontSize=40
kMenuFontSmallSize=32
kMenuButtonYStart=220
kMenuButtonYStartUp=190
kMenuButtonYDelta=kMenuFontSize+10
kMenuButtonYDeltaSmall=kMenuFontSmallSize
kMenuButtonYDeltaSmallest=kMenuFontSmallSize-5
kMainInfoOffset=120
kMultiInfoOffset=100

def get_pref_path():
    """ return the full path to config file, according to platform """
    if sys.platform=='darwin':
        try:
            fsRef=Carbon.Folder.FSFindFolder(-32763, 'pref', False)
            return os.path.join(Carbon.File.pathname(fsRef),kPrefFileName)
            
        except MacOS.Error:
            return None
    else:
        return kPrefFileName

def get_score_path():
    """ return the full path to config file, according to platform """
    if sys.platform=='darwin':
        try:
            fsRef=Carbon.Folder.FSFindFolder(-32763, 'pref', False)
            return os.path.join(Carbon.File.pathname(fsRef),kHighScoreFileName)
            
        except MacOS.Error:
            return None
    else:
        return kHighScoreFileName

def get_desktop_path():
    """ return the full path to config file, according to platform """
    if sys.platform=='darwin':
        try:
            fsRef=Carbon.Folder.FSFindFolder(-32763, 'desk', False)
            return Carbon.File.pathname(fsRef)
            
        except MacOS.Error:
            return None
    else:
        return kPrefFileName

def get_download_name(url):
    """ return the default download path, according to the OS """
    directory=get_desktop_path()
    name="%s%s%s" % (directory,os.sep,url.split("/")[-1])
    return name

def create_download(url):
    """ create a stream to store the url file """
    instream=urlopen(url, None)
    filename=instream.info().getheader("Content-Length")
    if filename==None:
        filename="temp"
    return (instream, filename)

